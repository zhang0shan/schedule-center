package com.dc;

import com.dc.schedule.api.model.ClientConfig;
import com.dc.schedule.api.model.StaticTaskRegistry;
import com.dc.schedule.api.model.StaticTaskRegistryConfig;
import com.dc.schedule.client.ScheduleClient;
import com.dc.schedule.client.concurrent.DefaultThreadFactory;
import com.dc.schedule.client.context.ClientExecutionContextManager;
import com.dc.schedule.client.context.ClientTaskExecutionContext;
import com.dc.schedule.client.socket.ScheduleSocketClient;
import com.dc.schedule.client.socket.SimpleScheduleSocketClient;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
public class Main4 {
    public static void main(String[] args) throws InterruptedException {
        final List<StaticTaskRegistryConfig> staticTaskRegistries = new ArrayList<>();
        StaticTaskRegistry registry = new StaticTaskRegistry();
        registry.setTaskDesc("测试任务2");
        registry.setTaskName("测试任务2");
        registry.setTriggerExpression("0/6 * * * * ?");
        StaticTaskRegistryConfig staticTaskRegistryConfig = new StaticTaskRegistryConfig();
        staticTaskRegistries.add(staticTaskRegistryConfig);
        staticTaskRegistryConfig.setRegistry(registry);
        staticTaskRegistryConfig.setRunnable(() -> {
            ClientTaskExecutionContext context = ClientExecutionContextManager.getINSTANCE().getCurrentExecutionContext();
            try {
                for (int i = 0; i < 5; i++) {
                    System.out.println(context.getFireTaskContext().getExecutionKey() + ": 测试任务 " + i);
                    Thread.sleep(1000L);
                }
            } catch (InterruptedException e) {
                log.error("任务执行终止", e);
            }
            context.setResultMessage(context.getFireTaskContext().getExecutionKey() + ": 测试任务 OK");
        });
        final ClientConfig clientConfig = new ClientConfig();
        clientConfig.setAppName("TEST-CLUSTER-2");
        clientConfig.setClientId("c-" + System.currentTimeMillis());
        // clientConfig.setAppName("TEST-CLUSTER");
        // clientConfig.setClientId("c3");
        clientConfig.setEndpoints(List.of(
                "localhost:2001",
                "localhost:2002",
                "localhost:2003"
        ));
        
        ScheduleSocketClient socketClient = new SimpleScheduleSocketClient();
        ScheduleClient client = new ScheduleClient(
                clientConfig, socketClient,
                new ThreadPoolExecutor(4, 16, 60L, TimeUnit.SECONDS, new SynchronousQueue<>(),
                                       new DefaultThreadFactory("task")
                ),
                staticTaskRegistries
        );
        try {
            client.start();
        } catch (Exception e) {
            client.shutdown();
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        client.shutdown();
        scanner = new Scanner(System.in);
        scanner.nextLine();
        
    }
}
