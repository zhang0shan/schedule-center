package com.dc.schedule.client.sample;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScheduleClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScheduleClientApplication.class, args);
    }
}
