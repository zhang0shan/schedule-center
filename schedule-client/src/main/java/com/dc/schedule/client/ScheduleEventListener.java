package com.dc.schedule.client;

import com.dc.schedule.api.model.SocketMessage;

public interface ScheduleEventListener {
    void onConnect();
    
    void onDisconnect();
    
    void onMessage(SocketMessage message);
}
