package com.dc.schedule.client.socket;

import com.dc.schedule.api.model.SocketMessage;
import com.dc.schedule.client.ScheduleEventListener;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;

public interface ScheduleSocketClient {
    void setEndpoints(List<InetSocketAddress> endpoints);
    
    void setScheduleEventListener(ScheduleEventListener listener);
    
    void sendMessage(SocketMessage socketMessage) throws IOException, InterruptedException;
    
    SocketMessage sendMessageForResponse(SocketMessage socketMessage) throws IOException, InterruptedException;
    
    void start();
    
    void disconnect();
    
    void shutdown();
}
