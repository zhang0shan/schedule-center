package com.dc.schedule.client.context;

import com.dc.schedule.api.context.FireTaskContext;
import com.dc.schedule.api.model.StaticTaskRegistryConfig;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.concurrent.Future;

/**
 * 任务客户端执行上下文
 */
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class ClientTaskExecutionContext {
    /**
     * 服务端提供的任务信息
     */
    private final FireTaskContext fireTaskContext;
    /**
     * 客户端自己的任务信息
     */
    private final StaticTaskRegistryConfig registry;
    /**
     * 任务线程的开关， 可以中断任务
     */
    private Future<?> future;
    /**
     * 返回结果, 可以设置值返回给服务端记录
     */
    private String resultMessage;
}
