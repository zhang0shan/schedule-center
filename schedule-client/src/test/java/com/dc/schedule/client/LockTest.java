package com.dc.schedule.client;

import com.dc.schedule.api.base.WaitResponseLock;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/27.
 */
public class LockTest {
    public static void main(String[] args) throws InterruptedException {
        final WaitResponseLock waitResponseLock = new WaitResponseLock();
        waitResponseLock.setWaitResponseTimeoutSeconds(3);
        new Thread(() -> {
            try {
                Thread.sleep(10000);
                waitResponseLock.setResponse("aaaaaaaa");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        System.out.println(waitResponseLock.getResponse());
    }
}
