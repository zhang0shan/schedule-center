package com.dc.schedule.client;

public class ThreadNameUniqueTest {
    /* 这个测试类证明线程的名称是不唯一的， 调度任务线程池的名称需要唯一！！*/
    public static void main(String[] args) throws InterruptedException {
        Thread a = new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    System.out.println("Thread a.name: " + Thread.currentThread().getName());
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "main");
        Thread b = new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    System.out.println("Thread b.name: " + Thread.currentThread().getName());
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "main");
        a.start();
        b.start();
        a.join();
        b.join();
        System.out.println("Exit!!!");
    }
}
