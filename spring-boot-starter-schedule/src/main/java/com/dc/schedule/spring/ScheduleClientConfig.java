package com.dc.schedule.spring;


import com.dc.schedule.api.model.ClientConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.autoconfigure.task.TaskExecutionProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotEmpty;
import java.util.List;


@Getter
@Setter
@ToString
@ConfigurationProperties("schedule.client")
public class ScheduleClientConfig extends ClientConfig {
    public ScheduleClientConfig() {
        setAppName("default");
        setClientId("default");
    }
    
    /**
     * 如果配置，将使用现成的线程池而不是直接创建，否则使用pool属性创建线程池
     * eg: applicationTaskExecutor
     */
    private String customExecutorServiceBeanName;
    
    private final TaskExecutionProperties.Pool pool = new TaskExecutionProperties.Pool();
    
    private final TaskExecutionProperties.Shutdown shutdown = new TaskExecutionProperties.Shutdown();
    
    private boolean enabled = true;
    
    @Override
    public void setAppName(@NotEmpty String appName) {
        super.setAppName(appName);
    }
    
    @Override
    public void setClientId(@NotEmpty String clientId) {
        super.setClientId(clientId);
    }
    
    @Override
    public void setEndpoints(@NotEmpty List<String> endpoints) {
        super.setEndpoints(endpoints);
    }
    
    
}
