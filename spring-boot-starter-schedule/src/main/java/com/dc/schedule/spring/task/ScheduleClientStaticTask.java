package com.dc.schedule.spring.task;

import com.dc.schedule.api.model.StaticTaskRegistryConfig;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.config.TriggerTask;

public class ScheduleClientStaticTask extends TriggerTask {
    private final StaticTaskRegistryConfig registry;
    
    public ScheduleClientStaticTask(StaticTaskRegistryConfig registry, Trigger trigger) {
        super(registry.getRunnable(), trigger);
        this.registry = registry;
    }
    
    @Override
    public String toString() {
        return "StaticTask: " + registry.getRegistry() + ", Runnable: " + registry.getRunnable();
    }
}
