package com.dc.schedule.spring.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 静态调度任务注解
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface StaticTask {
    /**
     * @return 任务名称， 需要具有唯一性 默认方法名
     */
    String name() default "";
    
    String description() default "";
    
    /**
     * 任务执行时间的表达式
     */
    String expression();
}
