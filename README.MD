# 调度中心

## 关于本项目

* 本项目是一个分布式调度中心, 做定时任务的
* 本项目期望达到简易配置定时任务, 提供界面检索和管理定时任务的目标
* 本项目的使用方法Demo在[spring-boot-starter-schedule](spring-boot-starter-schedule)  
  非常简单, 就一个类
* 本项目是服务端客户端模式, 需要启动启动Server端, 然后启动客户端
  *
  Server端 [ScheduleServerApplication.java](schedule-server%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdc%2Fschedule%2Fserver%2FScheduleServerApplication.java)
  *
  客户端 [ScheduleClientApplication.java](schedule-samples%2Fschedule-sample-spting-boot%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdc%2Fschedule%2Fclient%2Fsample%2FScheduleClientApplication.java)
  * 客户端 [Main4.java](schedule-samples%2Fschedule-sample-main%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdc%2FMain4.java)

* 启动前请注意修改数据库连接, 需要连一个数据库!

## 特性

* 用JPA制作, 不挑数据库
* 具备分布式高可用的特征， 即可以部署多个客户端和服务端
* 支持周期任务, 以及预期任务
* 开发友好, 注解式配置, 可以免页面操作, 和@Scheduled注解 一样方便

## 目前进度

3. Demo&Starter 就绪
2. Main测试类: `com.dc.schedule.client.DefaultScheduleClientImplTest`
1. 客户端和服务端通信成功

## 有XXL-JOB了， 为什么要做这个？

1. XXL-JOB的源代码对于二次开发非常不友好
2. XXL-JOB使用非mysql数据库需要耗费时间适配
3. XXL-JOB的UI不太美观, 操作不友好

## 目前版本处于孵化状态

1. 如何做到高可用？

* 客户端配置多个服务端地址， 然后随机选择一个进行长连接， 如果一个连接不上， 那就换一个连接， 直到连接上
  * 利用TCP原理， 保持连接的客户端即为在线状态， 一旦断开连接， 服务器就会抛出异常知道这个客户端断开了连接， 标记离线
  * 利用数据库锁（唯一键）对唯一执行的任务进行加锁， 加锁成功的服务端负责发起以及监控任务
   >
  * 对于服务端的高可用， 有以下两点
  * 服务端定时刷新数据库时间， 认为刷新时间在一定范围内的服务器处于存活状态， 这个对各个服务器对时有一定要求
  * 服务端定时刷新数据库的定时任务配置， 以数据库的为准

2. 如何收集客户端日志
  * XXL 通过定制Log类来收集定时任务日志
  * 我这边准备定制logback appender在做， 见 `com.dc.schedule.server.logback.LogTest`
  * 至于其他日志框架。。。之后再考虑

## TODO

* [x] 服务端锁占用
  `com/dc/schedule/server/service/task/impl/TaskManageServiceImpl.java:97`  
  即任务执行日志。。 先支持单例静态任务， 想好分片任务执行方式
* [x] 客户端定时任务上下文  
  用于获取任务信息*元数据* 以及便于日志保存
* [x] 客户端任务执行情况交互
* [x] 服务端定时任务执行状态管理 :记录任务执行状态， 以及停止任务？
* [x] 添加默认禁用的Cron表达式支持 - 写错误的cron表达式就不会执行
* [x] 定时刷新数据库配置的静态任务
* [x] spring-boot-starter
* [x] demo 代码
* [ ] 画一套UI
* [ ] 日志收集与实时展示
* [ ] 动态任务/分片任务、广播任务
* [ ] 立即执行, 立即停止
* [ ] 是否有必要对离线任务进行分析?
* [ ] 定时任务检查任务存活状态/可以合并心跳

---
现有问题

* refreshExecutionStatus 仍然有乐观锁异常 - 代码问题 - √
* refreshExecutionStatus 异常之后， 服务端断开连接， 客户端却没有发现， 需要建立心跳机制
* 需要考虑什么异常的情况下才断开连接 - 目前采用不中断连接的方式
* 同一个任务多次schedule的情况 - √ - 刷新调度任务的时候没有取消旧的调度任务

## 多实例测试证明

* ![](images/img.png)
* ![](images/img_1.png)
* ![](images/img_2.png)
