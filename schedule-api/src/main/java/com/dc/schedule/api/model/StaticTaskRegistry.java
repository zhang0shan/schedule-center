package com.dc.schedule.api.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class StaticTaskRegistry {
    private String taskName;
    private String taskDesc;
    private String triggerExpression;
}
