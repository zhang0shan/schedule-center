package com.dc.schedule.api.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
public class SocketClientRegistryMessage extends SocketMessage {
    public static final String THIS_TYPE = "client-registry";
    
    public SocketClientRegistryMessage() {
        this.type = THIS_TYPE;
    }
    
    private String clientId;
    private String appName;
    private List<StaticTaskRegistry> staticTasks;
}
