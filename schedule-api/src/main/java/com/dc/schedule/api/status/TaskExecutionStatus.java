package com.dc.schedule.api.status;

public enum TaskExecutionStatus {
    /**
     * 发起
     */
    FIRE,
    FIRED,
    /**
     * 发起失败, 极少见应该
     */
    FIRE_FAILED,
    /**
     * 执行中
     */
    EXECUTING,
    /**
     * 执行成功
     */
    EXECUTED,
    /**
     * 执行失败
     */
    EXECUTE_FAILED,
    /**
     * 客户端/服务端掉线, 丢失任务结果
     */
    MISSING
}
