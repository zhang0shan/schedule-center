package com.dc.schedule.api.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
public abstract class SocketMessageType {
    
    private static final Map<String, Class<? extends SocketMessage>> TYPE_MAP =
            new ConcurrentHashMap<>(Map.of(
                    SocketStatusMessage.THIS_TYPE, SocketStatusMessage.class,
                    SocketClientRegistryMessage.THIS_TYPE, SocketClientRegistryMessage.class,
                    SocketFireTaskMessage.THIS_TYPE, SocketFireTaskMessage.class,
                    ClientTaskNotifyMessage.THIS_TYPE, ClientTaskNotifyMessage.class
            ));
    
    public static Class<? extends SocketMessage> forType(String type) {
        return TYPE_MAP.get(type);
    }
    
    public static void addExternal(String type, Class<? extends SocketMessage> t) {
        TYPE_MAP.put(type, t);
    }
}
