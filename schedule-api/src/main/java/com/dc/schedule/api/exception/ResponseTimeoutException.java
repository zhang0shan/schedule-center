package com.dc.schedule.api.exception;

/***
 * descriptions...
 * @author Diamon.Cheng
 * @date 2022/8/3
 */
public class ResponseTimeoutException extends RuntimeException {
    public ResponseTimeoutException(String message) {
        super(message);
    }
}
