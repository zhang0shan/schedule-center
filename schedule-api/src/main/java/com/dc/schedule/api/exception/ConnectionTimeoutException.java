package com.dc.schedule.api.exception;

/***
 * descriptions...
 * @author Diamon.Cheng
 * @date 2022/8/3
 */
public class ConnectionTimeoutException extends RuntimeException {
    public ConnectionTimeoutException(String message) {
        super(message);
    }
}
