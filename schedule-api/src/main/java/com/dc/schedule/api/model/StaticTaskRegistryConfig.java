package com.dc.schedule.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/***
 * 任务名称和具体任务
 * @author Diamon.Cheng
 * @date 2022/10/24
 */
@Getter
@Setter
@ToString
public class StaticTaskRegistryConfig {
    private StaticTaskRegistry registry;
    private Runnable runnable;
}
