package com.dc.schedule.api.context;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/***
 * 调度任务发起信息
 * @author Diamon.Cheng
 * @date 2022/8/3
 */
@Getter
@Setter
@ToString
public class FireTaskContext {
    private String taskName;
    private Date expectFireTime;
    private Long executionId;
    private String executionKey;
    private String executionParam;
}
