package com.dc.schedule.api.model;

import com.dc.schedule.api.context.FireTaskContext;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/***
 * 调度任务消息
 * @author Diamon.Cheng
 * @date 2022/8/3
 */
@Getter
@Setter
@ToString(callSuper = true)
public class SocketFireTaskMessage extends SocketMessage {
    public static final String THIS_TYPE = "fire-task";
    private FireTaskContext taskContext;
    
    public SocketFireTaskMessage() {
        this.type = THIS_TYPE;
    }
}
