package com.dc.schedule.api.model;


import com.dc.schedule.api.status.TaskExecutionStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class ClientTaskNotifyMessage extends SocketMessage {
    public static final String THIS_TYPE = "client-task-status";
    
    public ClientTaskNotifyMessage() {
        this.type = THIS_TYPE;
    }
    
    private Long executionId;
    private TaskExecutionStatus status;
    private String message;
}
