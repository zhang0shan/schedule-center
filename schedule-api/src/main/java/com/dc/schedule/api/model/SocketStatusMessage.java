package com.dc.schedule.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@Getter
@Setter
@ToString(callSuper = true)
public class SocketStatusMessage extends SocketMessage {
    public static final String THIS_TYPE = "status";
    
    public SocketStatusMessage() {
        this.type = THIS_TYPE;
    }
    
    private boolean success = true;
    private String message;
    
    public static SocketStatusMessage ok(String requestUid) {
        final SocketStatusMessage socketStatusMessage = new SocketStatusMessage();
        socketStatusMessage.requestUid = requestUid;
        return socketStatusMessage;
    }
    
    public static SocketStatusMessage error(String requestUid, String message) {
        final SocketStatusMessage socketStatusMessage = new SocketStatusMessage();
        socketStatusMessage.success = false;
        socketStatusMessage.message = message;
        socketStatusMessage.requestUid = requestUid;
        return socketStatusMessage;
    }
}
