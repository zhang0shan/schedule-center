package com.dc.schedule.api.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientConfig {
    private String appName;
    private String clientId;
    private List<String> endpoints;
    
    public List<InetSocketAddress> getEndpoints() {
        return endpoints.stream().map(this::convertInetSocketAddr).collect(Collectors.toList());
    }
    
    private InetSocketAddress convertInetSocketAddr(String sourceString) {
        final String[] split = sourceString.split(":");
        if (split.length < 2) {
            throw new IllegalStateException("No Port Found in socket address: " + sourceString);
        }
        final String hostname = split[0];
        final int port = Integer.parseInt(split[1]);
        return new InetSocketAddress(hostname, port);
    }
}
