package com.dc.schedule.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@Getter
@Setter
@ToString
public abstract class SocketMessage {
    protected String requestUid;
    protected String type;
    private Map<String, String> headers;
}
