package com.dc.schedule.server.base;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.time.LocalDateTime;

/**
 * <p>业务基类
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@Getter
@Setter
@MappedSuperclass
@ToString
public abstract class BaseEntity {
    /**
     * 主键ID自动生成策略
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Comment("id主键")
    protected Long id;
    
    /**
     * 版本号
     */
    @Version
    @Column(name = "version")
    @Comment("乐观锁技术版本号")
    protected Integer version;
    
    /**
     * 创建时间
     */
    @Column(name = "create_date_time")
    @CreationTimestamp
    @Comment("创建时间")
    protected LocalDateTime createDateTime;
    
    /**
     * 最后修改时间
     */
    @Column(name = "update_date_time")
    @UpdateTimestamp
    @Comment("更新时间")
    protected LocalDateTime updateDateTime;
    
}
