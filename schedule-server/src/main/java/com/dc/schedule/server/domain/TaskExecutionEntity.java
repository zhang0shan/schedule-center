package com.dc.schedule.server.domain;

import com.dc.schedule.api.status.TaskExecutionStatus;
import com.dc.schedule.server.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Comment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "sc_task_execution")
@org.hibernate.annotations.Table(appliesTo = "sc_task_execution", comment = "定时任务执行记录表")
@Getter
@Setter
@ToString(callSuper = true)
public class TaskExecutionEntity extends BaseEntity {
    
    
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @Comment("对应的静态任务， 如果为空说明不是静态任务")
    private StaticTaskEntity staticTask;
    @Column(unique = true)
    @Comment("执行唯一键")
    private String executionKey;
    @Comment("执行参数JSON")
    @Column(length = 1024)
    private String executionParam;
    @Comment("执行客户端, AppName + ClientId")
    private String clientKey;
    
    @Comment("期望执行时间")
    private LocalDateTime expectedFireTime;
    @Comment("客户端响应确认时间")
    private LocalDateTime clientAckTime;
    @Comment("任务实际执行时间")
    private LocalDateTime completedTime;
    @Comment("当前状态 0 发起 1 已发起 2 发起失败 3 执行中 4 执行完毕 5 执行失败")
    private TaskExecutionStatus status;
    @Comment("执行(失败)之后的返回信息")
    @Column(length = 4096)
    private String message;
}
