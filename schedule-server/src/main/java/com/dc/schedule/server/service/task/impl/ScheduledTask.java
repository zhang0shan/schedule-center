package com.dc.schedule.server.service.task.impl;

import com.dc.schedule.server.service.client.impl.ClientHandler;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.SimpleTriggerContext;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

/***
 * descriptions...
 * @author Diamon.Cheng
 * @date 2022/8/3
 */
@Getter
@Setter
@ToString
public class ScheduledTask {
    private String taskKey;
    private String taskName;
    private ScheduledFuture<?> future;
    private Map<String, ClientHandler> clients;
    private CronTrigger trigger;
    private SimpleTriggerContext triggerContext;
    private int configVersion;
    private Date fireTime;
    
}
