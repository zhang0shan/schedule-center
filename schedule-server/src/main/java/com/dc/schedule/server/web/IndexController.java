package com.dc.schedule.server.web;

import com.dc.schedule.server.base.AjaxResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@RestController
public class IndexController {
    @GetMapping({"/", "/index"})
    public AjaxResult<Void> index() {
        return AjaxResult.ok();
    }
}
