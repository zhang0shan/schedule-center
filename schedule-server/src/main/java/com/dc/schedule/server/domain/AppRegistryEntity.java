package com.dc.schedule.server.domain;

import com.dc.schedule.server.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@Entity
@Table(name = "sc_app_registry")
@Getter
@Setter
@ToString(callSuper = true)
public class AppRegistryEntity extends BaseEntity {
    @Column(unique = true)
    private String appName;
}
