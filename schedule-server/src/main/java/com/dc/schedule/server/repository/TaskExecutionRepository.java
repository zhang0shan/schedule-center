package com.dc.schedule.server.repository;

import com.dc.schedule.api.status.TaskExecutionStatus;
import com.dc.schedule.server.domain.StaticTaskEntity;
import com.dc.schedule.server.domain.TaskExecutionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface TaskExecutionRepository extends JpaRepository<TaskExecutionEntity, Long> {
    List<TaskExecutionEntity> findByStaticTaskAndStatusIn(StaticTaskEntity staticTask, Set<TaskExecutionStatus> status);
}
