package com.dc.schedule.server.service.server;

import com.dc.schedule.server.domain.ServerRegistryEntity;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/26.
 */
public interface ServerRegistryService {
    ServerRegistryEntity getCurrentServerRegistry();
}
