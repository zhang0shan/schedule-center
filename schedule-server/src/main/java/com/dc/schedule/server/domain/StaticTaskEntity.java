package com.dc.schedule.server.domain;

import com.dc.schedule.server.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Comment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@Entity
@Table(name = "sc_static_task")
@Getter
@Setter
@ToString(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "sc_static_task", comment = "静态任务配置表")
public class StaticTaskEntity extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    @Comment("任务关联的应用")
    private AppRegistryEntity appRegistry;
    @Column
    @Comment("任务名称")
    private String taskName;
    @Column(length = 511)
    @Comment("任务描述")
    private String taskDesc;
    @Column(unique = true)
    @Comment("任务唯一键 appName#taskName")
    private String taskKey;
    @Column(length = 31)
    @Comment("任务cron表达式")
    private String triggerExpression;
    @Comment("是否启用")
    private Boolean enabled;
    @Comment("任务排期异常信息")
    private String errorMessage;
    @Comment("配置版本, enabled,triggerExpression 修改时+1")
    private Integer configVersion;
    @Comment("排期后下次触发的时间")
    private Date nextFireTime;
    @Comment("上次触发成功的时间")
    private Date lastFireTime;
}
