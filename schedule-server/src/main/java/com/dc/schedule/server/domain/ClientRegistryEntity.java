package com.dc.schedule.server.domain;

import com.dc.schedule.server.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@Entity
@Table(name = "sc_client_registry")
@Getter
@Setter
@ToString(callSuper = true)
public class ClientRegistryEntity extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    private AppRegistryEntity appRegistry;
    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    private ServerRegistryEntity serverRegistry;
    @Column(unique = true)
    private String clientKey;
    private String clientId;
    @ManyToMany(fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<StaticTaskEntity> staticTasks;
}
