package com.dc.schedule.server.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@Configuration
@ConfigurationProperties("schedule.server")
@Getter
@Setter
@ToString
public class ServerConfig {
    public static final int OPTIMISTIC_LOCK_RETRY_COUNT =3;
    /**
     * 每5秒刷新服务器状态
     */
    private int heartbeatRateSeconds = 5;
    private int taskRefreshRateSeconds = 5;
    private String serverId = "default";
    private String socketBindHost = "0.0.0.0";
    private int socketListenPort = 2333;
    private int selectorThreads = 1;
    /**
     * 访问服务器的IP地址， 用于服务器内部通讯
     */
    private String accessUrl;
    private int corePoolCoreSize = 4;
    private int corePoolMaxSize = 16;
    
    public int getServerDeadHeartbeatTimeBeforeSeconds(){
        return heartbeatRateSeconds*2;
    }
}
