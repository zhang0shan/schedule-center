package com.dc.schedule.server.service.client;

import com.dc.schedule.api.model.SocketMessage;

public interface ClientMessageConvertor {
    /**
     * 字符串转换成对象
     *
     * @param json json数据
     * @return 对象
     */
    SocketMessage decode(String json);
    
    /**
     * 对象转换成 json字符串
     *
     * @param message 对象
     * @return 字符串
     */
    String encode(SocketMessage message);
}
