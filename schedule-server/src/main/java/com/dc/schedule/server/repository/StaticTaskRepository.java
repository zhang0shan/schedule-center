package com.dc.schedule.server.repository;

import com.dc.schedule.server.domain.StaticTaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/26.
 */
@Repository
public interface StaticTaskRepository extends JpaRepository<StaticTaskEntity, Long> {
    StaticTaskEntity getByTaskKey(String taskKey);
    
    List<StaticTaskEntity> findAllByTaskKeyIn(Collection<String> taskKeys);
}
