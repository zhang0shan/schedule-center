package com.dc.schedule.server.repository;

import com.dc.schedule.server.domain.ClientRegistryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/26.
 */
@Repository
public interface ClientRegistryRepository extends JpaRepository<ClientRegistryEntity, Long> {
    ClientRegistryEntity getByClientKey(String clientKey);
}
