package com.dc.schedule.server.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public abstract class ClientConnectionEvent {
    protected String innerClientId;
}
