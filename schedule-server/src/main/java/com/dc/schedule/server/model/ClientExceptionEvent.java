package com.dc.schedule.server.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class ClientExceptionEvent extends ClientConnectionEvent {
    private String requestUid;
    private Exception cause;
    
}
