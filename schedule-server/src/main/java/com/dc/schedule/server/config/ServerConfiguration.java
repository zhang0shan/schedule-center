package com.dc.schedule.server.config;

import com.dc.schedule.server.service.client.ClientMessageConvertor;
import com.dc.schedule.server.service.client.impl.JacksonClientMessageConvertorImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@Configuration
@RequiredArgsConstructor
public class ServerConfiguration {
    private final ServerConfig serverConfig;
    
    @Bean()
    @Qualifier("clientEventLoopPool")
    public ThreadPoolTaskExecutor clientEventLoopPool() {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(1);
        executor.setMaxPoolSize(1);
        executor.setThreadGroupName("client-event");
        executor.setThreadNamePrefix("client-event");
        executor.setDaemon(true);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setQueueCapacity(0);
        return executor;
    }
    
    @Bean
    @Qualifier("coreThreadExecutor")
    public ThreadPoolTaskExecutor coreThreadExecutor() {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(serverConfig.getCorePoolCoreSize());
        executor.setMaxPoolSize(serverConfig.getCorePoolMaxSize());
        executor.setThreadGroupName("schedule-core");
        executor.setThreadNamePrefix("schedule-core");
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setQueueCapacity(0);
        return executor;
    }
    
    @Bean
    public ClientMessageConvertor clientMessageConvertor(ObjectMapper objectMapper) {
        return new JacksonClientMessageConvertorImpl(objectMapper);
    }
}
