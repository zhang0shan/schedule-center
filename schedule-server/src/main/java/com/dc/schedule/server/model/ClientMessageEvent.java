package com.dc.schedule.server.model;

import com.dc.schedule.api.model.SocketMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class ClientMessageEvent extends ClientConnectionEvent {
    protected SocketMessage socketMessage;
}
