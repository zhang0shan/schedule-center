package com.dc.schedule.server.repository;

import com.dc.schedule.server.domain.AppRegistryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/26.
 */
@Repository
public interface AppRegistryRepository extends JpaRepository<AppRegistryEntity, Long> {
    AppRegistryEntity getByAppName(String appName);
}
