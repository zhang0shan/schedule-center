package com.dc.schedule.server.base;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@Getter
@Setter
@ToString
public class AjaxResult<T> {
    private boolean success = true;
    private String message;
    private T data;
    
    public static AjaxResult<Void> ok() {
        return new AjaxResult<>();
    }
    
    public static <T> AjaxResult<T> okData(T data) {
        final AjaxResult<T> result = new AjaxResult<>();
        result.setData(data);
        return result;
    }
}
