package com.dc.schedule.server.service.client;

import com.dc.schedule.api.model.SocketMessage;
import com.dc.schedule.server.model.ClientConnectionEvent;
import org.springframework.lang.Nullable;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
public interface ClientConnectionService {
    void sendMessage(String innerClientId, SocketMessage message);
    
    @Nullable
    ClientConnectionEvent takeEvent();
    
    void disconnect(String innerClientId);
}
