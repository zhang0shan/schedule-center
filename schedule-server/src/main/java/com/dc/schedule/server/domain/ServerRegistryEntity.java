package com.dc.schedule.server.domain;

import com.dc.schedule.server.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/26.
 */
@Entity
@Table(name = "sc_server_registry")
@Getter
@Setter
@ToString(callSuper = true)
public class ServerRegistryEntity extends BaseEntity {
    @Column(unique = true)
    private String serverId;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "serverRegistry")
    @ToString.Exclude
    private List<ClientRegistryEntity> clientRegistries;
    private String serverWebEndpoint;
    @UpdateTimestamp
    private LocalDateTime heartbeatTime;
}
