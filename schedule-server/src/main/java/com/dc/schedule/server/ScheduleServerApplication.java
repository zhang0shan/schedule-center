package com.dc.schedule.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * <p>Descriptions...
 *
 * @author Diamon.Cheng
 * @date 2022/7/23.
 */
@SpringBootApplication
@EnableScheduling
public class ScheduleServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScheduleServerApplication.class, args);
    }
}
