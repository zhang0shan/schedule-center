package com.dc.schedule.server.service.task;

import com.dc.schedule.api.status.TaskExecutionStatus;
import com.dc.schedule.server.domain.StaticTaskEntity;
import com.dc.schedule.server.service.client.impl.ClientHandler;

import java.util.List;

/***
 * descriptions...
 * @author Diamon.Cheng
 * @date 2022/8/3
 */
public interface TaskManageService {
    /**
     * 注册客户端的调度器
     *
     * @param staticTasks   任务列表
     * @param clientHandler 客户端回调
     */
    void registerClientTask(List<StaticTaskEntity> staticTasks, ClientHandler clientHandler);
    
    /**
     * 取消注册客户端, 如果一个定时任务没有客户端了, 那就消除掉定时任务
     *
     * @param clientKey 客户端的键
     */
    void unregister(String clientKey);
    
    void refreshExecutionStatus(Long executionId, TaskExecutionStatus status, String message);
}
