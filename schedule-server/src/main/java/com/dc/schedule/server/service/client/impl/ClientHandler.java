package com.dc.schedule.server.service.client.impl;

import com.dc.schedule.api.context.FireTaskContext;
import lombok.AllArgsConstructor;
import lombok.Getter;

/***
 * descriptions...
 * @author Diamon.Cheng
 * @date 2022/8/3
 */
@Getter
@AllArgsConstructor
public abstract class ClientHandler {
    private final String clientKey;
    
    public abstract void fireTask(FireTaskContext context);
}
