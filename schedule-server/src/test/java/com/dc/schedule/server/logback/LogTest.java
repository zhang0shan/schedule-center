package com.dc.schedule.server.logback;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.slf4j.ILoggerFactory;
import org.slf4j.impl.StaticLoggerBinder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@SpringBootTest
@Slf4j
public class LogTest {
    @Value("${logging.charset.file:UTF-8}")
    private Charset fileCharset = StandardCharsets.UTF_8;
    @Value("${logging.file.pattern:%d{yyyy-MM-dd HH:mm:ss.SSS} ${LOG_LEVEL_PATTERN:-%5p} ${PID:- } --- [%t] %-40.40logger{39} : %m%n}")
    private String filePattern = "%d{yyyy-MM-dd HH:mm:ss.SSS} ${LOG_LEVEL_PATTERN:-%5p} ${PID:- } --- [%t] %-40.40logger{39} : %m%n";
    
    @Test
    public void startAddAppender() throws Exception {
        
        LoggerContext logbackContext = getLoggerContext();
        Logger logger = logbackContext.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        FileAppender<ILoggingEvent> fileAppender = new FileAppender<>();
        fileAppender.setFile("test.log");
        fileAppender.setName("Additional Custom Appender");
        fileAppender.setContext(logbackContext);
        fileAppender.setAppend(true);
        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setPattern(filePattern);
        encoder.setCharset(fileCharset);
        encoder.setContext(logbackContext);
        encoder.start();
        fileAppender.setEncoder(encoder);
        fileAppender.start();
        logger.addAppender(fileAppender);
        
        for (int i = 0; i < 10; i++) {
            log.info("[啊啊啊啊] {}", i);
            Thread.sleep(1000);
        }
        log.info("[啊啊啊啊] end");
    }
    
    // logback
    private LoggerContext getLoggerContext() {
        ILoggerFactory factory = StaticLoggerBinder.getSingleton().getLoggerFactory();
        return (LoggerContext) factory;
    }
}
